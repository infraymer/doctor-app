package com.example.photohandler.domain.form.model

data class FormData(
    val id: String,
    val title: String,
    val info: String,
    val type: FormType,
    val value: Any,
    val field: Any? = null
)