package com.example.photohandler.domain.form.model

data class ToggleForm(
    val firstButton: ToggleButtonData,
    val secondButton: ToggleButtonData
)