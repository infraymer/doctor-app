package com.example.photohandler.domain.kid.model

data class Param<T>(
    val id: String,
    val value: T
)