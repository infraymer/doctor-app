package com.example.photohandler.domain.kid.model

data class Kid(
    val id: String = "",
    val params: List<Param<*>> = emptyList(),
    val images: List<Image> = emptyList()
)