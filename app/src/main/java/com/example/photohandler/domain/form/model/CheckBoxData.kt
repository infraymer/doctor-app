package com.example.photohandler.domain.form.model

data class CheckBoxData(
    val name: String,
    val value: Boolean = false
)