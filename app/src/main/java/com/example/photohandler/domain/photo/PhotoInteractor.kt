package com.example.photohandler.domain.photo

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.graphics.drawable.toBitmap
import com.example.photohandler.view.base.extension.dip
import io.reactivex.Single
import java.io.File
import java.io.FileOutputStream
import java.util.*
import javax.inject.Inject

class PhotoInteractor
@Inject
constructor(
    private val context: Context
) {

    fun blurFast(bmp: Bitmap, radius: Int): Bitmap {
        val w = bmp.width
        val h = bmp.height
        val pix = IntArray(w * h)
        bmp.getPixels(pix, 0, w, 0, 0, w, h)
        var r = radius
        while (r >= 1) {
            for (i in r until h - r) {
                for (j in r until w - r) {
                    val tl = pix[(i - r) * w + j - r]
                    val tr = pix[(i - r) * w + j + r]
                    val tc = pix[(i - r) * w + j]
                    val bl = pix[(i + r) * w + j - r]
                    val br = pix[(i + r) * w + j + r]
                    val bc = pix[(i + r) * w + j]
                    val cl = pix[i * w + j - r]
                    val cr = pix[i * w + j + r]
                    pix[i * w + j] = -0x1000000 or (
                            (tl and 0xFF) + (tr and 0xFF) + (tc and 0xFF) + (bl and 0xFF) + (br and 0xFF) + (bc and 0xFF) + (cl and 0xFF) + (cr and 0xFF) shr 3 and 0xFF) or (
                            (tl and 0xFF00) + (tr and 0xFF00) + (tc and 0xFF00) + (bl and 0xFF00) + (br and 0xFF00) + (bc and 0xFF00) + (cl and 0xFF00) + (cr and 0xFF00) shr 3 and 0xFF00) or (
                            (tl and 0xFF0000) + (tr and 0xFF0000) + (tc and 0xFF0000) + (bl and 0xFF0000) + (br and 0xFF0000) + (bc and 0xFF0000) + (cl and 0xFF0000) + (cr and 0xFF0000) shr 3 and 0xFF0000)
                }
            }
            r /= 2
        }
        bmp.setPixels(pix, 0, w, 0, 0, w, h)
        return bmp
    }

    fun cutOval(bitmap: Bitmap): Bitmap {
        val width = (bitmap.width * 0.7).toInt()
        val height = (bitmap.height * 0.25).toInt()
        val x = (bitmap.width - width) / 2
        val y = (bitmap.height - height) / 2
        val newBitmap = Bitmap.createBitmap(bitmap, x, y, width, height)
        val rbd = RoundedBitmapDrawableFactory.create(context.resources, newBitmap)
        rbd.cornerRadius = context.dip(200f)
        return rbd.toBitmap()
    }

    fun mergeToPin(back: Bitmap, front: Bitmap): Bitmap? {
        val result =
            Bitmap.createBitmap(back.width, back.height, back.config)
        val canvas = Canvas(result)
        val widthBack = back.width
        val widthFront = front.width
        val moveX = (widthBack - widthFront) / 2.toFloat()
        val moveY = (back.height - front.height) / 2.toFloat()
        canvas.drawBitmap(back, 0f, 0f, null)
        canvas.drawBitmap(front, moveX, moveY, null)
        return result
    }

    fun savePhotoToCache(bitmap: Bitmap): Single<String> =
        Single.fromCallable {
            val path = context.cacheDir.path
            val fileName = UUID.randomUUID().toString()
            val file = File(path, fileName)
            val output = FileOutputStream(file.absolutePath)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output)
            output.flush()
            output.close()
            file.path
        }
}