package com.example.photohandler.domain.form.model

enum class FormType {
    TOGGLE,
    NUMBER_PICKER,
    CHECKBOX_LIST
}