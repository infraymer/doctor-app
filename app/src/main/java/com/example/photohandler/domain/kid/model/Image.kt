package com.example.photohandler.domain.kid.model

data class Image(
    val id: String,
    val uri: String,
    val isSick: Boolean = false
)