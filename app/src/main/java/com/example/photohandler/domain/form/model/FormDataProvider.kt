package com.example.photohandler.domain.form.model

import com.example.photohandler.R
import java.util.*

object FormDataProvider {

    fun createForms(): List<FormData> =
        listOf(
            FormData(
                id = UUID.randomUUID().toString(),
                title = "Пол ребенка",
                info = "Укажите пол ребенка",
                type = FormType.TOGGLE,
                value = 0,
                field = ToggleForm(
                    ToggleButtonData(
                        color = R.color.color_woman,
                        icon = null,
                        text = "Женский"
                    ),
                    ToggleButtonData(
                        color = R.color.color_man,
                        icon = null,
                        text = "Мужской"
                    )
                )
            ),
            FormData(
                id = UUID.randomUUID().toString(),
                title = "Возраст ребенка",
                info = "Укажите возраст ребенка",
                type = FormType.NUMBER_PICKER,
                value = 0
            ),
            FormData(
                id = UUID.randomUUID().toString(),
                title = "Чем болен ребенок?",
                info = "Признаки какого заболевания обнаружены?",
                type = FormType.CHECKBOX_LIST,
                value = 0,
                field = listOf(
                    CheckBoxData("Простудное заболевание"),
                    CheckBoxData("Конъюнктивит"),
                    CheckBoxData("Аллергия")
                )
            ),
            FormData(
                id = UUID.randomUUID().toString(),
                title = "Как ты?",
                info = "Как себя ощущаешь?",
                type = FormType.TOGGLE,
                value = 0,
                field = ToggleForm(
                    ToggleButtonData(
                        color = R.color.color_sample_2,
                        icon = R.drawable.ic_thumb_down,
                        text = "Неоч"
                    ),
                    ToggleButtonData(
                        color = R.color.color_sample_1,
                        icon =  R.drawable.ic_thumb_up,
                        text = "Огонь"
                    )
                )
            )
        )
}