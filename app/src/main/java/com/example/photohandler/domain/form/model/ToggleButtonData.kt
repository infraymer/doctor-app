package com.example.photohandler.domain.form.model

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes

data class ToggleButtonData(
    @ColorRes val color: Int,
    @DrawableRes val icon: Int?,
    val text: String
)