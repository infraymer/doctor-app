package com.example.photohandler

import com.example.photohandler.domain.kid.model.Image
import com.example.photohandler.domain.kid.model.Kid
import java.util.*

object NewKidState {

    var kid = Kid()

    val hasNextPhoto: Boolean
        get() = kid.images.size < 3

    fun addPhoto(path: String, isSick: Boolean) {
        val list = kid.images.toMutableList()
        list.add(Image(UUID.randomUUID().toString(), path, isSick))
        kid = kid.copy(images = list)
    }
}