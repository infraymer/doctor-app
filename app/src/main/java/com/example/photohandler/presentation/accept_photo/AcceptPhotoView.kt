package com.example.photohandler.presentation.accept_photo

import android.graphics.Bitmap

interface AcceptPhotoView {
    sealed class State {
        object Idle : State()
        object Loading : State()
        object Error : State()
        data class Content(val bitmap: Bitmap) : State()
    }

    fun setState(state: State)
    fun showMessage(text: String)
}