package com.example.photohandler.presentation.kid_list

import com.example.photohandler.view.base.AppSchedulers
import com.example.photohandler.view.form.navigation.screen.FormScreen
import com.example.photohandler.view.make_photo.navigation.screen.MakePhotoScreen
import ru.nobird.android.presentation.base.PresenterBase
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class KidListPresenter
@Inject
constructor(
    private val router: Router,
    private val schedulers: AppSchedulers
) : PresenterBase<KidListView>() {
    private var state: KidListView.State = KidListView.State.Idle
        set(value) {
            field = value
            view?.setState(value)
        }

    override fun attachView(view: KidListView) {
        super.attachView(view)
        view.setState(state)
    }

    fun onAddClicked() {
//        router.newChain(MakePhotoScreen)
        router.newChain(FormScreen)
    }
}