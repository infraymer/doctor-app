package com.example.photohandler.presentation.kid_list

import com.example.photohandler.domain.kid.model.Kid


interface KidListView {
    sealed class State {
        object Idle : State()
        object Loading : State()
        data class Content(val kids: List<Kid> = emptyList()) : State()
    }

    fun setState(state: State)
}