package com.example.photohandler.presentation.accept_photo

import com.example.photohandler.Bus
import com.example.photohandler.NewKidState
import com.example.photohandler.domain.photo.PhotoInteractor
import com.example.photohandler.view.base.AppSchedulers
import com.example.photohandler.view.form.navigation.screen.FormScreen
import com.example.photohandler.view.make_photo.navigation.screen.MakePhotoScreen
import io.reactivex.Single
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.nobird.android.presentation.base.PresenterBase
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class AcceptPhotoPresenter
@Inject
constructor(
    private val router: Router,
    private val photoInteractor: PhotoInteractor,
    private val schedulers: AppSchedulers
) : PresenterBase<AcceptPhotoView>() {
    private var state: AcceptPhotoView.State = AcceptPhotoView.State.Idle
        set(value) {
            field = value
            view?.setState(value)
        }

    init {
        state = AcceptPhotoView.State.Loading
        compositeDisposable += Single.fromCallable {
            val bitmap = Bus.image!!
            val oval = photoInteractor.cutOval(bitmap)
            val blur = photoInteractor.blurFast(bitmap, 60)
            val image = photoInteractor.mergeToPin(blur, oval)
            image!!
        }
            .subscribeOn(schedulers.io)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onSuccess = {
                    state = AcceptPhotoView.State.Content(it)
                },
                onError = {
                    state = AcceptPhotoView.State.Error
                }
            )
    }

    override fun attachView(view: AcceptPhotoView) {
        super.attachView(view)
        view.setState(state)
    }

    fun onAcceptClicked(isSick: Boolean) {
        val oldState = state as? AcceptPhotoView.State.Content ?: return
        compositeDisposable += photoInteractor
            .savePhotoToCache(oldState.bitmap)
            .subscribeOn(schedulers.io)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onSuccess = {
                    NewKidState.addPhoto(it, isSick)
                    if (NewKidState.hasNextPhoto) {
                        router.finishChain()
                        router.newChain(MakePhotoScreen)
                    } else {
                        router.navigateTo(FormScreen)
                    }
                },
                onError = {
                    view?.showMessage("Ошибка при сохранении фото")
                }
            )
    }
}