package com.example.photohandler.presentation.form

import com.example.photohandler.domain.form.model.FormData


interface FormView {
    sealed class State {
        object Idle : State()
        object Loading : State()

        data class Content(
            val forms: List<FormData>,
            val currentForm: Int = 0
        ) : State()
    }

    fun setState(state: State)
}