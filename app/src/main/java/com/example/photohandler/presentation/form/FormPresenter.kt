package com.example.photohandler.presentation.form

import com.example.photohandler.domain.form.model.FormDataProvider
import com.example.photohandler.presentation.form.mapper.FormStateMapper
import com.workingeeks.yoursport.view.injection.qualifiers.BackgroundScheduler
import com.workingeeks.yoursport.view.injection.qualifiers.MainScheduler
import io.reactivex.Scheduler
import ru.nobird.android.presentation.base.PresenterBase
import javax.inject.Inject

class FormPresenter
@Inject
constructor(
    private val formStateMapper: FormStateMapper,

    @BackgroundScheduler
    private val backgroundScheduler: Scheduler,
    @MainScheduler
    private val mainScheduler: Scheduler
) : PresenterBase<FormView>() {
    private var _state: FormView.State = FormView.State.Content(
        FormDataProvider.createForms()
    )
    private var state: FormView.State
        get() = _state
        set(value) {
            _state = value
            view?.setState(value)
        }

    override fun attachView(view: FormView) {
        super.attachView(view)
        view.setState(state)
    }

    fun onChangeValue(value: Any, forceUpdate: Boolean = true) {
        val oldState = state as? FormView.State.Content
            ?: return
    }

    fun onNextClicked() {
        val oldState = state as? FormView.State.Content
            ?: return
        if (isFinish(oldState))
            finishForm()
        state = oldState.copy(currentForm = oldState.currentForm + 1)
    }

    private fun isFinish(state: FormView.State.Content): Boolean =
        state.currentForm + 1 >= state.forms.size

    private fun finishForm() {

    }
}