package com.example.photohandler.presentation.make_photo

interface MakePhotoView {
    sealed class State {
        object Idle : State()
        object Loading : State()
        object Complete : State()
    }

    fun setState(state: State)
}