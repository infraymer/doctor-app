package com.example.photohandler.presentation.make_photo

import android.graphics.Bitmap
import com.example.photohandler.Bus
import com.example.photohandler.view.accept_photo.navigation.screen.AcceptPhotoScreen
import com.workingeeks.yoursport.view.injection.qualifiers.BackgroundScheduler
import com.workingeeks.yoursport.view.injection.qualifiers.MainScheduler
import io.reactivex.Scheduler
import ru.nobird.android.presentation.base.PresenterBase
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MakePhotoPresenter
@Inject
constructor(
    private val router: Router,
    @BackgroundScheduler
    private val backgroundScheduler: Scheduler,
    @MainScheduler
    private val mainScheduler: Scheduler
) : PresenterBase<MakePhotoView>() {
    private var state: MakePhotoView.State = MakePhotoView.State.Idle
        set(value) {
            field = value
            view?.setState(value)
        }

    override fun attachView(view: MakePhotoView) {
        super.attachView(view)
        view.setState(state)
    }

    fun onPhotoTaken(bitmap: Bitmap?) {
        state = MakePhotoView.State.Complete
        Bus.image = bitmap
        router.navigateTo(AcceptPhotoScreen)
    }

    fun onTakePhotoClicked() {
        state = MakePhotoView.State.Loading
    }

    fun onViewCreated() {
        state = MakePhotoView.State.Idle
    }
}