package com.example.photohandler

import android.app.Activity
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import jp.wasabeef.glide.transformations.CropTransformation
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.synthetic.main.activity_sample.*
import java.io.IOException
import java.io.OutputStream


class SampleActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE_TAKE_PHOTO = 102
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)

        imagePickButton.setOnClickListener {
            val intent = Intent().apply {
                type = "*/*"
                action = Intent.ACTION_GET_CONTENT
            }
            startActivityForResult(intent, 100)
        }

        takePhotoButton.setOnClickListener {
            startActivityForResult(Intent(this, CameraActivity::class.java), REQUEST_CODE_TAKE_PHOTO)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 100) {
            if (data?.data != null) {
                val uri = data.data ?: return
                val image = contentResolver.openInputStream(uri)
                val bitmap = BitmapFactory.decodeStream(image)

                Glide.with(this)
                    .asBitmap()
                    .load(bitmap)
                    .transform(
                        MultiTransformation<Bitmap>(
                            CropTransformation(
                                dip(300),
                                dip(150),
                                CropTransformation.CropType.CENTER
                            ),
                            RoundedCornersTransformation(
                                dip(200),
                                0
                            )
                        )
                    )
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {

                            onFun(resource)
                            imageView.setImageBitmap(resource)

//                            saveBitmap(this@MainActivity, resource, CompressFormat.JPEG, "image/jpeg", "EXPE")
//                            Toast.makeText(this@MainActivity, "Good", Toast.LENGTH_SHORT).show()
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            // this is called when imageView is cleared on lifecycle call or for
                            // some other reason.
                            // if you are referencing the bitmap somewhere else too other than this imageView
                            // clear it here as you can no longer have the bitmap
                        }
                    })
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_TAKE_PHOTO) {
            imageView.setImageBitmap(Bus.image)
        }
    }

    fun Context.dip(int: Int): Int =
        (int * resources.displayMetrics.density).toInt()

    fun Context.dip(float: Float): Float =
        (float * resources.displayMetrics.density)

    fun onFun(bitmap: Bitmap) {
        saveBitmap(this, bitmap, CompressFormat.JPEG, "image/jpeg", "Exp")
        Toast.makeText(this@SampleActivity, "Saved", Toast.LENGTH_SHORT).show()
    }

    @Throws(IOException::class)
    private fun saveBitmap(
        context: Context, bitmap: Bitmap,
        format: CompressFormat, mimeType: String,
        displayName: String
    ) {
        val relativeLocation: String = Environment.DIRECTORY_PICTURES
        val contentValues = ContentValues()
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, displayName)
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, mimeType)
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, relativeLocation)
        val resolver: ContentResolver = context.contentResolver
        var stream: OutputStream? = null
        var uri: Uri? = null
        try {
            val contentUri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            uri = resolver.insert(contentUri, contentValues)
            if (uri == null) {
                throw IOException("Failed to create new MediaStore record.")
            }
            stream = resolver.openOutputStream(uri)
            if (stream == null) {
                throw IOException("Failed to get output stream.")
            }
            if (!bitmap.compress(format, 95, stream)) {
                throw IOException("Failed to save bitmap.")
            }
        } catch (e: IOException) {
            if (uri != null) {
                resolver.delete(uri, null, null)
            }
            throw e
        } finally {
            stream?.close()
        }
    }
}