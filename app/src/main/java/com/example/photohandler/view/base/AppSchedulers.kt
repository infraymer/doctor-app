package com.example.photohandler.view.base

import io.reactivex.Scheduler

interface AppSchedulers {
    val ui: Scheduler
    val io: Scheduler
}