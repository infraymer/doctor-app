package com.example.photohandler.view.injection.make_photo

import com.example.photohandler.view.make_photo.ui.fragment.MakePhotoFragment
import dagger.Subcomponent

@Subcomponent(
    modules = [
        MakePhotoModule::class
    ]
)
interface MakePhotoComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): MakePhotoComponent
    }

    fun inject(fragment: MakePhotoFragment)
}