package com.example.photohandler.view.injection.app

import com.example.photohandler.view.base.navigation.AppRouter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module
internal class AppNavigationModule {
    private val cicerone: Cicerone<Router> = Cicerone.create(AppRouter())

    @ApplicationScope
    @Provides
    internal fun provideRouter(): Router =
        cicerone.router

    @ApplicationScope
    @Provides
    internal fun provideNavigatorHolder(): NavigatorHolder =
        cicerone.navigatorHolder
}