package com.example.photohandler.view.form.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.photohandler.App
import com.example.photohandler.R
import com.example.photohandler.domain.form.model.FormType
import com.example.photohandler.presentation.form.FormPresenter
import com.example.photohandler.presentation.form.FormView
import com.example.photohandler.view.form.ui.delegate.CheckBoxFormDelegate
import com.example.photohandler.view.form.ui.delegate.NumberPickerFormDelegate
import com.example.photohandler.view.form.ui.delegate.ToggleFormDelegate
import kotlinx.android.synthetic.main.fragment_form.*
import ru.nobird.android.ui.viewstatedelegate.ViewStateDelegate
import javax.inject.Inject

class FormFragment : Fragment(), FormView {
    companion object {
        fun newInstance(): Fragment =
            FormFragment()
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var formPresenter: FormPresenter
    private lateinit var viewStateDelegate: ViewStateDelegate<FormView.State>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        injectComponent()

        formPresenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(FormPresenter::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_form, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        backButton.setOnClickListener { }
        viewStateDelegate = ViewStateDelegate()

        form_next_button.setOnClickListener { formPresenter.onNextClicked() }
    }

    private fun injectComponent() {
        App.component()
            .formComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onStart() {
        super.onStart()
        formPresenter.attachView(this)
    }

    override fun onStop() {
        formPresenter.detachView(this)
        super.onStop()
    }

    override fun setState(state: FormView.State) {
        when (state) {
            is FormView.State.Content -> {
                val data = state.forms[state.currentForm]
                form_title.text = data.title
                form_info.text = data.info
                form_container.removeAllViews()
                when (data.type) {
                    FormType.TOGGLE -> {
                        ToggleFormDelegate(requireView()).apply {
                            setData(data)
                        }
                    }
                    FormType.NUMBER_PICKER -> {
                        NumberPickerFormDelegate(requireView())
                    }
                    FormType.CHECKBOX_LIST -> {
                        CheckBoxFormDelegate(requireView()).apply {
                            setData(data)
                        }
                    }
                    else -> ""
                }
            }
        }
    }
}