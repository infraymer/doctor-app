package com.example.photohandler.view.base.navigation

import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.BackTo

class AppRouter : Router() {

    override fun finishChain() {
        executeCommands(
            BackTo(null)
        )
    }
}