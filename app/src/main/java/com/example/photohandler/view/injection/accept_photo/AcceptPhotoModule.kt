package com.example.photohandler.view.injection.accept_photo

import androidx.lifecycle.ViewModel
import com.example.photohandler.presentation.accept_photo.AcceptPhotoPresenter
import com.example.photohandler.presentation.make_photo.MakePhotoPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.nobird.android.view.injection.base.presentation.ViewModelKey

@Module
abstract class AcceptPhotoModule {

    @Binds
    @IntoMap
    @ViewModelKey(AcceptPhotoPresenter::class)
    internal abstract fun bindAcceptPhotoPresenter(acceptPhotoPresenter: AcceptPhotoPresenter): ViewModel
}