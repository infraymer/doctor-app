package com.workingeeks.yoursport.view.injection.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class MainScheduler