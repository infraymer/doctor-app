package com.example.photohandler.view.form.ui.delegate

import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import com.example.photohandler.R
import com.example.photohandler.domain.form.model.FormData
import com.example.photohandler.domain.form.model.ToggleForm
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_form.*
import kotlinx.android.synthetic.main.layout_toggle.*

class ToggleFormDelegate(
    override val containerView: View
) : LayoutContainer {

    init {
        LayoutInflater.from(containerView.context)
            .inflate(R.layout.layout_toggle, form_container, true)
        card_1.setOnClickListener {
            card_1.select()
            card_2.deselect()
        }
        card_2.setOnClickListener {
            card_1.deselect()
            card_2.select()
        }
    }

    private fun View.select() =
        animate()
            .scaleX(1f)
            .scaleY(1f)
            .alpha(1f)
            .setDuration(300)
            .start()

    private fun View.deselect() =
        animate()
            .scaleX(0.9f)
            .scaleY(0.9f)
            .alpha(0.7f)
            .setDuration(300)
            .start()

    fun setData(formData: FormData) {
        val field = formData.field as? ToggleForm ?: return
        card_1.setCardBackgroundColor(
            ContextCompat.getColor(
                containerView.context,
                field.firstButton.color
            )
        )
        card_2.setCardBackgroundColor(
            ContextCompat.getColor(
                containerView.context,
                field.secondButton.color
            )
        )
        field.firstButton.icon?.let(icon_1::setImageResource)
        field.secondButton.icon?.let(icon_2::setImageResource)
        title_1.text = field.firstButton.text
        title_2.text = field.secondButton.text
    }

}