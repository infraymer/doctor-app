package com.example.photohandler.view.base.extension

import android.content.Context

fun Context.dip(int: Int): Int =
    (int * resources.displayMetrics.density).toInt()

fun Context.dip(float: Float): Float =
    (float * resources.displayMetrics.density)