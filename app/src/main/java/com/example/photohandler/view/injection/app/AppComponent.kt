package com.example.photohandler.view.injection.app

import android.content.Context
import com.example.photohandler.view.injection.accept_photo.AcceptPhotoComponent
import com.example.photohandler.view.injection.form.FormComponent
import com.example.photohandler.view.injection.kid_list.KidListComponent
import com.example.photohandler.view.injection.main.MainComponent
import com.example.photohandler.view.injection.make_photo.MakePhotoComponent
import dagger.BindsInstance
import dagger.Component

@ApplicationScope
@Component(
    modules = [
        AppModule::class,
        AppNavigationModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun context(context: Context): Builder
    }

    fun mainComponentBuilder(): MainComponent.Builder

    fun formComponentBuilder(): FormComponent.Builder

    fun makePhotoComponentBuilder(): MakePhotoComponent.Builder

    fun acceptPhotoComponentBuilder(): AcceptPhotoComponent.Builder

    fun kidListComponentBuilder(): KidListComponent.Builder
}