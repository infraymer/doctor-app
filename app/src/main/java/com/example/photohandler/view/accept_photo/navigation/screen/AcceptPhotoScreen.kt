package com.example.photohandler.view.accept_photo.navigation.screen

import androidx.fragment.app.Fragment
import com.example.photohandler.view.accept_photo.ui.fragment.AcceptPhotoFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object AcceptPhotoScreen : SupportAppScreen() {
    override fun getFragment(): Fragment =
        AcceptPhotoFragment.newInstance()
}