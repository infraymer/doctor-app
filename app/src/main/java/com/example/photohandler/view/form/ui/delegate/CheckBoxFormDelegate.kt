package com.example.photohandler.view.form.ui.delegate

import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.LinearLayoutCompat
import com.example.photohandler.R
import com.example.photohandler.domain.form.model.CheckBoxData
import com.example.photohandler.domain.form.model.FormData
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_form.*

class CheckBoxFormDelegate(
    override val containerView: View
) : LayoutContainer {

    private val formView: LinearLayoutCompat =
        LayoutInflater.from(containerView.context)
            .inflate(R.layout.layout_form_checkbox, null) as LinearLayoutCompat

    init {
        form_container.addView(formView)
    }

    fun setData(formData: FormData) {
        val field = formData.field as? List<CheckBoxData> ?: return
        field.forEach { generateCheckBox(it) }
    }

    private fun generateCheckBox(checkBoxData: CheckBoxData) {
        val checkBox = LayoutInflater.from(containerView.context)
            .inflate(R.layout.layout_checkbox, null) as AppCompatCheckBox
        checkBox.isChecked = checkBoxData.value
        checkBox.text = checkBoxData.name
        formView.addView(checkBox)
    }
}