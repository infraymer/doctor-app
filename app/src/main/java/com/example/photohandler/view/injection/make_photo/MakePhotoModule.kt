package com.example.photohandler.view.injection.make_photo

import androidx.lifecycle.ViewModel
import com.example.photohandler.presentation.make_photo.MakePhotoPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.nobird.android.view.injection.base.presentation.ViewModelKey

@Module
abstract class MakePhotoModule {

    @Binds
    @IntoMap
    @ViewModelKey(MakePhotoPresenter::class)
    internal abstract fun bindMakePhotoPresenter(makePhotoPresenter: MakePhotoPresenter): ViewModel
}