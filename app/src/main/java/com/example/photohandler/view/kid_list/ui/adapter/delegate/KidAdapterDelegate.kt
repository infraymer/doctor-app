package com.example.photohandler.view.kid_list.ui.adapter.delegate

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import com.example.photohandler.R
import com.example.photohandler.view.kid_list.model.KidListItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_kid.*
import ru.nobird.android.ui.adapterdelegates.AdapterDelegate
import ru.nobird.android.ui.adapterdelegates.DelegateViewHolder

class PlaceFavoriteAdapterDelegate(
    var onClick: (KidListItem.Main) -> Unit = {}
) : AdapterDelegate<KidListItem, DelegateViewHolder<KidListItem>>() {

    override fun onCreateViewHolder(parent: ViewGroup): DelegateViewHolder<KidListItem> =
        ViewHolder(createView(parent, R.layout.item_kid))

    override fun isForViewType(position: Int, data: KidListItem): Boolean =
        data is KidListItem.Main

    private inner class ViewHolder(
        override val containerView: View
    ) : DelegateViewHolder<KidListItem>(containerView), LayoutContainer {

        init {
            containerView.setOnClickListener { (itemData as? KidListItem.Main)?.let(onClick) }
        }

        @SuppressLint("SetTextI18n")
        override fun onBind(data: KidListItem) {
            data as KidListItem.Main
            title.text = "Пациент #${data.value.id}"
        }
    }
}