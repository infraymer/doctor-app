package com.example.photohandler.view.kid_list.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.photohandler.App
import com.example.photohandler.R
import com.example.photohandler.presentation.kid_list.KidListPresenter
import com.example.photohandler.presentation.kid_list.KidListView
import com.example.photohandler.view.kid_list.model.KidListItem
import com.example.photohandler.view.kid_list.ui.adapter.delegate.PlaceFavoriteAdapterDelegate
import kotlinx.android.synthetic.main.fragment_kid_list.*
import ru.nobird.android.ui.adapters.DefaultDelegateAdapter
import ru.nobird.android.ui.viewstatedelegate.ViewStateDelegate
import javax.inject.Inject

class KidListFragment : Fragment(), KidListView {
    companion object {
        fun newInstance(): Fragment =
            KidListFragment()
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var kidListPresenter: KidListPresenter
    private lateinit var kidListItemadapter: DefaultDelegateAdapter<KidListItem>
    private lateinit var viewStateDelegate: ViewStateDelegate<KidListView.State>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        injectComponent()

        kidListPresenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(KidListPresenter::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_kid_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewStateDelegate = ViewStateDelegate()
        kidListItemadapter = DefaultDelegateAdapter()
        kidListItemadapter += PlaceFavoriteAdapterDelegate { }
        recyclerView.adapter = kidListItemadapter
        addButton.setOnClickListener { kidListPresenter.onAddClicked() }
    }

    private fun injectComponent() {
        App.component()
            .kidListComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onStart() {
        super.onStart()
        kidListPresenter.attachView(this)
    }

    override fun onStop() {
        kidListPresenter.detachView(this)
        super.onStop()
    }

    override fun setState(state: KidListView.State) {
        when (state) {
            is KidListView.State.Content -> {
                kidListItemadapter.items = state.kids.map { KidListItem.Main(it) }
            }
        }
    }
}