package com.example.photohandler.view.injection.main

import com.example.photohandler.view.form.ui.fragment.FormFragment
import com.example.photohandler.view.main.ui.activity.MainActivity
import dagger.Subcomponent

@Subcomponent(
    modules = []
)
interface MainComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): MainComponent
    }

    fun inject(activity: MainActivity)
}