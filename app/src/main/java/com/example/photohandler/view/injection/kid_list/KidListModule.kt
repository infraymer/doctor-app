package com.example.photohandler.view.injection.kid_list

import androidx.lifecycle.ViewModel
import com.example.photohandler.presentation.kid_list.KidListPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.nobird.android.view.injection.base.presentation.ViewModelKey

@Module
abstract class KidListModule {

    @Binds
    @IntoMap
    @ViewModelKey(KidListPresenter::class)
    internal abstract fun bindKidListPresenter(kidListPresenter: KidListPresenter): ViewModel
}