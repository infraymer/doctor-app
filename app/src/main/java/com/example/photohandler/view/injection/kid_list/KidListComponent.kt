package com.example.photohandler.view.injection.kid_list

import com.example.photohandler.view.kid_list.ui.fragment.KidListFragment
import dagger.Subcomponent

@Subcomponent(
    modules = [
        KidListModule::class
    ]
)
interface KidListComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): KidListComponent
    }

    fun inject(fragment: KidListFragment)
}