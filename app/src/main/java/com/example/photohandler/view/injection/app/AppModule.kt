package com.example.photohandler.view.injection.app

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.example.photohandler.base.Config
import com.example.photohandler.base.storage.AppStorage
import com.example.photohandler.view.base.AppSchedulers
import com.workingeeks.yoursport.view.injection.qualifiers.BackgroundScheduler
import com.workingeeks.yoursport.view.injection.qualifiers.MainScheduler
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.nobird.android.view.injection.base.presentation.DaggerViewModelFactory

@Module
abstract class AppModule {
    @Binds
    internal abstract fun bindViewModelFactory(daggerViewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Module
    companion object {

        @Provides
        @JvmStatic
        internal fun provideConfig(): Config =
            Config(
                baseUrl = "https://tvoysport.com/api/"
            )

        @Provides
        @JvmStatic
        @MainScheduler
        internal fun provideAndroidScheduler(): Scheduler =
            AndroidSchedulers.mainThread()

        @Provides
        @JvmStatic
        @BackgroundScheduler
        internal fun provideBackgroundScheduler(): Scheduler =
            Schedulers.io()

        @Provides
        @JvmStatic
        internal fun provideSchedulers(): AppSchedulers =
            object : AppSchedulers {
                override val ui: Scheduler = AndroidSchedulers.mainThread()
                override val io: Scheduler = Schedulers.io()
            }

        @Provides
        @JvmStatic
        internal fun provideAppStorage(): AppStorage =
            AppStorage

        @Provides
        @JvmStatic
        internal fun provideResources(context: Context): Resources =
            context.resources
    }
}