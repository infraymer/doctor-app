package com.example.photohandler.view.injection.accept_photo

import com.example.photohandler.view.accept_photo.ui.fragment.AcceptPhotoFragment
import com.example.photohandler.view.make_photo.ui.fragment.MakePhotoFragment
import dagger.Subcomponent

@Subcomponent(
    modules = [
        AcceptPhotoModule::class
    ]
)
interface AcceptPhotoComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): AcceptPhotoComponent
    }

    fun inject(fragment: AcceptPhotoFragment)
}