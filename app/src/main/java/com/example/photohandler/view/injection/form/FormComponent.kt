package com.example.photohandler.view.injection.form

import com.example.photohandler.view.form.ui.fragment.FormFragment
import dagger.Subcomponent

@Subcomponent(
    modules = [
        FormModule::class
    ]
)
interface FormComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): FormComponent
    }

    fun inject(fragment: FormFragment)
}