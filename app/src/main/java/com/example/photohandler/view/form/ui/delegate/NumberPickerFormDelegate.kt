package com.example.photohandler.view.form.ui.delegate

import android.view.LayoutInflater
import android.view.View
import com.example.photohandler.R
import com.example.photohandler.domain.form.model.FormData
import com.example.photohandler.domain.form.model.ToggleForm
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_form.*

class NumberPickerFormDelegate(
    override val containerView: View
) : LayoutContainer {

    init {
        LayoutInflater.from(containerView.context)
            .inflate(R.layout.layout_number_picker, form_container, true)
    }

    fun setData(formData: FormData) {
        val field = formData.field as? ToggleForm ?: return
    }

}