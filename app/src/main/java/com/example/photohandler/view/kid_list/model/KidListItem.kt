package com.example.photohandler.view.kid_list.model

import com.example.photohandler.domain.kid.model.Kid

sealed class KidListItem {
    data class Main(val value: Kid) : KidListItem()
}