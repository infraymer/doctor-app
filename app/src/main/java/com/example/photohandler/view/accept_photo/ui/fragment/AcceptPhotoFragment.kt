package com.example.photohandler.view.accept_photo.ui.fragment

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.photohandler.App
import com.example.photohandler.R
import com.example.photohandler.presentation.accept_photo.AcceptPhotoPresenter
import com.example.photohandler.presentation.accept_photo.AcceptPhotoView
import com.example.photohandler.view.base.AppSchedulers
import com.example.photohandler.view.base.extension.dip
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_accept_photo.*
import ru.nobird.android.ui.viewstatedelegate.ViewStateDelegate
import ru.nobird.android.view.base.ui.extension.snackbar
import javax.inject.Inject


class AcceptPhotoFragment : DialogFragment(), AcceptPhotoView {
    companion object {
        fun newInstance(): Fragment =
            AcceptPhotoFragment()
    }

    @Inject
    internal lateinit var schedulers: AppSchedulers

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var makePhotoPresenter: AcceptPhotoPresenter
    private lateinit var viewStateDelegate: ViewStateDelegate<AcceptPhotoView.State>

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialogStyle)
        injectComponent()
        makePhotoPresenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(AcceptPhotoPresenter::class.java)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        super.onCreateDialog(savedInstanceState)
            .apply {
                window?.requestFeature(Window.FEATURE_NO_TITLE)
                setCancelable(false)
                setCanceledOnTouchOutside(false)
            }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_accept_photo, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewStateDelegate = ViewStateDelegate()
        viewStateDelegate.addState<AcceptPhotoView.State.Loading>(progressBar, cancelButton)
        viewStateDelegate.addState<AcceptPhotoView.State.Error>(errorTextView, cancelButton)
        viewStateDelegate.addState<AcceptPhotoView.State.Content>(
            image_view,
            cancelButton,
            badButton,
            goodButton
        )

        badButton.setOnClickListener { makePhotoPresenter.onAcceptClicked(true) }
        goodButton.setOnClickListener { makePhotoPresenter.onAcceptClicked(false) }
        cancelButton.setOnClickListener { activity?.onBackPressed() }
    }

    private fun injectComponent() {
        App.component()
            .acceptPhotoComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onStart() {
        super.onStart()
        makePhotoPresenter.attachView(this)
    }

    override fun onStop() {
        makePhotoPresenter.detachView(this)
        super.onStop()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun setState(state: AcceptPhotoView.State) {
        viewStateDelegate.switchState(state)
        when (state) {
            is AcceptPhotoView.State.Content -> {
                Glide.with(this)
                    .load(state.bitmap)
                    .into(image_view)
            }
        }
    }

    override fun showMessage(text: String) {
        view?.snackbar(text)
    }
}