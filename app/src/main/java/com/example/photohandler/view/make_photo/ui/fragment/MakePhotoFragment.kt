package com.example.photohandler.view.make_photo.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.photohandler.App
import com.example.photohandler.R
import com.example.photohandler.presentation.make_photo.MakePhotoPresenter
import com.example.photohandler.presentation.make_photo.MakePhotoView
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import kotlinx.android.synthetic.main.fragment_make_photo.*
import ru.nobird.android.ui.viewstatedelegate.ViewStateDelegate
import javax.inject.Inject


class MakePhotoFragment : Fragment(), MakePhotoView {
    companion object {
        fun newInstance(): Fragment =
            MakePhotoFragment()
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var makePhotoPresenter: MakePhotoPresenter
    private lateinit var viewStateDelegate: ViewStateDelegate<MakePhotoView.State>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()
        makePhotoPresenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(MakePhotoPresenter::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_make_photo, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewStateDelegate = ViewStateDelegate()
        viewStateDelegate.addState<MakePhotoView.State.Idle>(area, camera, pick)
        viewStateDelegate.addState<MakePhotoView.State.Loading>(progressBar)

        pick.setOnClickListener {
            camera.takePicture()
            makePhotoPresenter.onTakePhotoClicked()
        }

        val viewTreeObserver = camera.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    val lp = area.layoutParams
                    lp.height = (camera.height * 0.2).toInt()
                    area.layoutParams = lp
                }
            })
        }

        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                result.toBitmap(result.size.width, result.size.height) {
                    makePhotoPresenter.onPhotoTaken(it)
                }
            }
        })

        makePhotoPresenter.onViewCreated()
    }

    private fun injectComponent() {
        App.component()
            .makePhotoComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onStart() {
        super.onStart()
        makePhotoPresenter.attachView(this)
    }

    override fun onResume() {
        super.onResume()
        camera?.open()
    }

    override fun onPause() {
        super.onPause()
        camera?.close()
    }

    override fun onStop() {
        makePhotoPresenter.detachView(this)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        camera?.destroy()
    }

    override fun setState(state: MakePhotoView.State) {
        viewStateDelegate.switchState(state)
    }
}