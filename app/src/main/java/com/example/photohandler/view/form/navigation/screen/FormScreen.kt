package com.example.photohandler.view.form.navigation.screen

import androidx.fragment.app.Fragment
import com.example.photohandler.view.form.ui.fragment.FormFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object FormScreen : SupportAppScreen() {
    override fun getFragment(): Fragment =
        FormFragment.newInstance()
}