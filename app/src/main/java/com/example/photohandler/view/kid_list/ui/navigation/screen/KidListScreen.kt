package com.example.photohandler.view.kid_list.ui.navigation.screen

import androidx.fragment.app.Fragment
import com.example.photohandler.view.kid_list.ui.fragment.KidListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object KidListScreen : SupportAppScreen() {
    override fun getFragment(): Fragment =
        KidListFragment.newInstance()
}