package com.example.photohandler.view.injection.form

import androidx.lifecycle.ViewModel
import com.example.photohandler.presentation.form.FormPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.nobird.android.view.injection.base.presentation.ViewModelKey

@Module
abstract class FormModule {

    @Binds
    @IntoMap
    @ViewModelKey(FormPresenter::class)
    internal abstract fun bindFormPresenter(formPresenter: FormPresenter): ViewModel
}