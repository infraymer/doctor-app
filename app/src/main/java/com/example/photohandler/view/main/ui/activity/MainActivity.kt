package com.example.photohandler.view.main.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.photohandler.App
import com.example.photohandler.R
import com.example.photohandler.view.form.navigation.screen.FormScreen
import com.example.photohandler.view.kid_list.ui.navigation.screen.KidListScreen
import com.example.photohandler.view.make_photo.navigation.screen.MakePhotoScreen
import ru.nobird.android.view.navigation.ui.listener.BackButtonListener
import ru.nobird.android.view.navigation.ui.listener.BackNavigable
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class MainActivity : AppCompatActivity(), BackNavigable {

    @Inject
    internal lateinit var navigatorHolder: NavigatorHolder

    @Inject
    internal lateinit var router: Router

    private val navigator = SupportAppNavigator(this, supportFragmentManager, R.id.main_container)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            router.newRootScreen(KidListScreen)
        }
    }

    private fun injectComponent() {
        App.component()
            .mainComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        (supportFragmentManager.findFragmentById(R.id.main_container) as? BackButtonListener)
            ?.onBackPressed()
            ?: router.exit()
    }

    override fun onNavigateBack() {
        router.exit()
    }
}