package com.example.photohandler.view.injection.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class StethoInterceptor