package com.example.photohandler.view.make_photo.navigation.screen

import androidx.fragment.app.Fragment
import com.example.photohandler.view.make_photo.ui.fragment.MakePhotoFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object MakePhotoScreen : SupportAppScreen() {
    override fun getFragment(): Fragment =
        MakePhotoFragment.newInstance()
}