package com.example.photohandler

import android.app.Application
import com.example.photohandler.view.injection.app.AppComponent
import com.example.photohandler.view.injection.app.DaggerAppComponent
import com.example.photohandler.view.util.StethoHelper
import com.example.photohandler.view.injection.app.ComponentManager
import ru.nobird.android.view.base.ui.extension.isMainProcess

class App : Application() {

    companion object {
        lateinit var application: App
            private set

        fun component(): AppComponent =
            application.component

        fun componentManager(): ComponentManager =
            application.componentManager
    }

    private lateinit var component: AppComponent
    private lateinit var componentManager: ComponentManager

    override fun onCreate() {
        super.onCreate()
        if (!isMainProcess) return

        StethoHelper.initStetho(this)

        application = this
        component = DaggerAppComponent.builder()
            .context(application)
            .build()
        componentManager = ComponentManager(component)
    }
}